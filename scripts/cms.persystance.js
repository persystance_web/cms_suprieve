var language_id = 1; 
var language_name = '';
var language_code = '';

if(!localStorage.getItem("cms-language_id")||!localStorage.getItem("cms-language_name")||!localStorage.getItem("cms-language_code")){ 
$.getJSON("settings.json", function(json) { 
language_id = json.languages[0].language_id; 
language_name = json.languages[0].name; 
language_code = json.languages[0].language_code; 
localStorage.setItem("cms-language_id", json.languages[0].language_id); 
localStorage.setItem("cms-language_name", json.languages[0].name); 
localStorage.setItem("cms-language_code", json.languages[0].language_code); 
}); 
} 
else{ 
language_id = localStorage.getItem("cms-language_id"); 
language_name = localStorage.getItem("cms-language_name"); 
language_code = localStorage.getItem("cms-language_code"); 
} 

var page_name = 'index'; 
var url = window.location.href; 
var page = url.split(domain)[1]; 
if(page.split('/')[1]){page = page.split('/')[1];}else{page='index';} 
if(page.split("?")[0]){page = page.split("?")[0];}else{page='index';} 
if(page&&page!=''){page_name = page;}else{page_name = 'index';} 

// alert(getUrlParameterByName('ln')); 

checkLanguageAndRediret(); 
function checkLanguageAndRediret(){ 
var url = window.location.href; 
var urlend = url.split(domain)[1]; 
var urllntxt = urlend.split('/')[0]; 
$.getJSON("settings.json", function(json) { 
$.each(json.languages, function(i, language){ 
if(language.language_id == language_id){ 
if(language.language_code != urllntxt){ 
urlend = urlend.replace(urllntxt+'/',''); 
window.location.replace(domain+language.language_code+'/'+urlend); 
} 
}
}); 
});
} 

function pageRedirect(relurl,linkOpenType = ""){ 
$.getJSON("settings.json", function(json) { 
$.each(json.languages, function(i, language){ 
if(language.language_id == language_id){ 
    if(linkOpenType=="") {
        window.location.replace(language.language_code+'/'+relurl);
    }else{
        window.open(language.language_code+'/'+relurl,linkOpenType);
    }
}
}); 
}); 
} 

$( document ).ready(async function() { 
await loadPage(); 
setPageTitle(); 
}); 

function loadPage(){ 
$.ajax({ 
headers: {"language_id":language_id},
url: api_url+'portal/page/to/element/details?page_name='+page_name, 
type: 'GET',
success: async function (ptoc) { 
console.log(ptoc);
for (i = 0; i < ptoc.length; i++) {  
$('#cms-page-content').append('<div id="ptoc'+ptoc[i].page_to_element_data[0]._id+'"></div>'); 
await loadptocdata(ptoc[i]); 
}
}, 
}); 
}

function loadptocdata(ptocdata){
var data = {}
var element_data = {}
if(ptocdata.page_to_element_data[0].data){data = JSON.parse(ptocdata.page_to_element_data[0].data);}
if(ptocdata.element.element_data){element_data = JSON.parse(ptocdata.element.element_data);} 
$.get( 'components/'+ptocdata.element.unique_id+'.html' , async function(element, status){ 
var div_id = ptocdata.page_to_element_data[0]._id; 
await renderTemplate(element,data,element_data,div_id);
}); 
}

function renderTemplate(element,data,element_data,div_id){ 
var template = eval('`'+element+'`'); 
$('#ptoc'+div_id).append(template); 
}

function setPageTitle(){ 
$.ajax({ 
url: api_url+'portal/page/details?name='+page_name, 
type: 'GET',
success: function (response) { 
console.log(response); 
if(JSON.parse(response[0].page_title)[language_id]){document.title = JSON.parse(response[0].page_title)[language_id];} 
}, 
}); 
}
