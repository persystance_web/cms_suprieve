$.ajaxSetup ({ cache: false }); 

var api_url = 'http://3.136.49.247:3048/api/v1/'; 
var domain = 'http://localhost/cms_suprieve/'; 
// var domain = 'http://www.persystance.com/cms_suprieve/'; 

// var cms_config = {'app_id':'5ee866aab9beda1d7bf4a036'}; 

// var base = document.createElement('base');
// base.href = domain;
// document.getElementsByTagName('head')[0].appendChild(base);

$( document ).ready(function() {
$("#header").load("components/common/header.html"); 
$("#footer").load("components/common/footer.html"); 
});

function getUrlParameterByName( name, url ){ 
if (!url) url = location.href;
name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
var regexS = "[\\?&]"+name+"=([^&#]*)";
var regex = new RegExp( regexS );
var results = regex.exec( url );
return results == null ? null : results[1];
} 

function formatDateyyyymmdd(dateStr="") {
var date = new Date(dateStr);
return date.getFullYear() + '-' + ((date.getMonth() > 8) ? (date.getMonth() + 1) : ('0' + (date.getMonth() + 1))) + '-' + ((date.getDate() > 9) ? date.getDate() : ('0' + date.getDate()));
} 


// Preloader js
$("#status").fadeOut();
$(".preloader").delay(1000).fadeOut("slow");
$("body").css('overflow-y', 'visible');
$("body").css('position', 'relative'); 


// Wow js
wow = new WOW(
{
boxClass:     'wow',      // default
animateClass: 'animated', // default
offset:       0,          // default
mobile:       false,       // default
live:         true        // default
}
)
wow.init();

$(document).ready(function(){ 
setTimeout(function(){ 
$('#cms-page-content').css('min-height', '0px'); 
}, 3000); 
});


