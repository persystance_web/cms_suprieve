$.ajaxSetup ({ cache: false }); 

var api_url = 'http://3.136.49.247:3048/api/v1/'; 
// var domain = 'http://localhost/cms_suprieve/cms_admin'; 

$( document ).ready(function() {
$("#top-head").load("components/common/header.html"); 
$("#left-side-bar").load("components/common/left_side_bar.html"); 
}); 


if(!localStorage.getItem("cms-language_id")){
$.getJSON("../settings.json", function(json) { 
localStorage.setItem("cms-language_id", json.languages[0].language_id);
});
}

function getUrlParameterByName( name, url ){ 
if (!url) url = location.href;
name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
var regexS = "[\\?&]"+name+"=([^&#]*)";
var regex = new RegExp( regexS );
var results = regex.exec( url );
return results == null ? null : results[1];
} 

function formatDateyyyymmdd(dateStr="") { 
var date = new Date(dateStr);
return date.getFullYear() + '-' + ((date.getMonth() > 8) ? (date.getMonth() + 1) : ('0' + (date.getMonth() + 1))) + '-' + ((date.getDate() > 9) ? date.getDate() : ('0' + date.getDate()));
} 

function getSettings(){
$.getJSON("../settings.json", function(json) { 
return json;
});
}



